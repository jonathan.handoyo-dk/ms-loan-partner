@file:Suppress("ReactiveStreamsUnusedPublisher")

package com.jago.loan.partner.controllers

import com.jago.loan.partner.RANDOM
import com.jago.loan.partner.controllers.dtos.GetPartnerResponse
import com.jago.loan.partner.domains.Partner
import com.jago.loan.partner.nextObjects
import com.jago.loan.partner.services.PartnerService
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.http.ProblemDetail
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.reactive.server.WebTestClient
import org.springframework.test.web.reactive.server.expectBody
import org.springframework.test.web.reactive.server.expectBodyList
import reactor.core.publisher.Flux
import reactor.kotlin.core.publisher.toFlux

@WebFluxTest
@ExtendWith(SpringExtension::class)
internal class PartnerControllerTests(
  @Autowired private val webTestClient: WebTestClient
) {

  @MockkBean private lateinit var partnerService: PartnerService

  @Nested
  inner class GetAllPartners {

    @Test
    fun givenPartners_shouldRespond200_andPartnersBody() {
      // Given
      val count = RANDOM.nextInt(10)
      val partners = RANDOM.nextObjects<Partner>(count)
      every { partnerService.getAllPartners() } returns partners.toFlux()

      // When-Then
      val response = partners.map { GetPartnerResponse(it) }
      webTestClient
        .get()
        .uri("/partners")
        .exchange()
        .expectStatus().is2xxSuccessful
        .expectBodyList<GetPartnerResponse>()
        .hasSize(response.size)
        .contains(*response.toTypedArray())
    }

    @Test
    fun givenNoPartner_shouldRespond404_andProblemDetailBody() {
      // Given
      every { partnerService.getAllPartners() } returns Flux.empty()

      // When-Then
      webTestClient
        .get()
        .uri("/partners")
        .exchange()
        .expectStatus().isNotFound
        .expectBody<ProblemDetail>()
    }

    @Test
    fun givenError_shouldRespond500_andProblemDetailBody() {
      // Given
      every { partnerService.getAllPartners() } returns Flux.error(Exception("Boom!"))

      // When-Then
      webTestClient
        .get()
        .uri("/partners")
        .exchange()
        .expectStatus().is5xxServerError
        .expectBody<ProblemDetail>()
    }
  }

  @Nested
  inner class GetPartnerById {

    @Test
    fun givenPartnerWithId_shouldRespond200_andPartnerBody() {}

    @Test
    fun givenNoPartnerWithId_shouldRespond404_andProblemDetailBody() {}

    @Test
    fun givenError_shouldRespond500_andProblemDetailBody() {}
  }

  @Nested
  inner class PostPartner {

    @Test
    fun givenNonExistingPartner_shouldRespond200_andPartnerBody() {}

    @Test
    fun givenExistingPartner_shouldRespond409_andProblemDetailBody() {}

    @Test
    fun givenError_shouldRespond500_andProblemDetailBody() {}
  }

  @Nested
  inner class PatchPartnerEmail {

    @Test
    fun givenExistingPartner_shouldRespond200_andPartnerBody() {}

    @Test
    fun givenNonExistingPartner_shouldRespond409_andProblemDetailBody() {}

    @Test
    fun givenError_shouldRespond500_andProblemDetailBody() {}
  }

  @Nested
  inner class PatchPartnerAccount {

  }

  @Nested
  inner class GetPartnerAccounts {

  }

}
