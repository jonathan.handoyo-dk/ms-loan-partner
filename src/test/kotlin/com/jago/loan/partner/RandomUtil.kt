package com.jago.loan.partner

import org.jeasy.random.EasyRandom

val RANDOM = EasyRandom()

inline fun <reified T> EasyRandom.nextObject(): T = RANDOM.nextObject(T::class.java)
inline fun <reified T> EasyRandom.nextObjects(count: Int): List<T> = (1..count).map { RANDOM.nextObject() }
