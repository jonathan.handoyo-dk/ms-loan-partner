@file:Suppress("ReactiveStreamsUnusedPublisher")

package com.jago.loan.partner.services

import com.jago.loan.partner.RANDOM
import com.jago.loan.partner.domains.Partner
import com.jago.loan.partner.domains.PartnerRepository
import com.jago.loan.partner.nextObject
import com.jago.loan.partner.nextObjects
import io.mockk.every
import io.mockk.junit5.MockKExtension
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono
import reactor.test.StepVerifier

@ExtendWith(MockKExtension::class)
internal class PartnerServiceTests {

  private val partnerRepository = mockk<PartnerRepository>()

  private val partnerService = PartnerService(
    partnerRepository
  )

  @Nested
  inner class GetAllPartners {

    @Test
    fun givenPartners_shouldReturnNonEmptyFlux() {
      // Given
      val count = RANDOM.nextInt(10)
      every { partnerRepository.findAll() } returns RANDOM.nextObjects<Partner>(count).toFlux()

      // When-Then
      StepVerifier
        .create(partnerService.getAllPartners())
        .expectSubscription()
        .expectNextCount(count.toLong())
        .expectComplete()
        .verify()
    }

    @Test
    fun givenNoPartners_shouldReturnEmptyFlux() {
      // Given
      every { partnerRepository.findAll() } returns Flux.empty()

      // When-Then
      StepVerifier
        .create(partnerService.getAllPartners())
        .expectSubscription()
        .expectComplete()
        .verify()
    }

    @Test
    fun givenErrorWhileRetrieving_shouldReturnErrorFlux() {
      // Given
      every { partnerRepository.findAll() } returns Flux.error(Exception("Boom!"))

      // When-Then
      StepVerifier
        .create(partnerService.getAllPartners())
        .expectSubscription()
        .expectErrorSatisfies {
          assertEquals(Exception::class, it::class)
          assertEquals("Boom!", it.message)
        }
        .verify()
    }
  }

  @Nested
  inner class GetPartnerById {

    @Test
    fun givenPartnerWithId_shouldReturnNonEmptyMono() {
      // Given
      val partner = RANDOM.nextObject<Partner>()
      every { partnerRepository.findById(partner.id) } returns partner.toMono()

      // When-Then
      StepVerifier
        .create(partnerService.getPartnerById(partner.id))
        .expectSubscription()
        .expectNext(partner)
        .expectComplete()
        .verify()
    }

    @Test
    fun givenNoPartnerWithId_shouldReturnEmptyMono() {
      // Given
      every { partnerRepository.findById(any<String>()) } returns Mono.empty()

      // When-Then
      StepVerifier
        .create(partnerService.getPartnerById(RANDOM.nextObject()))
        .expectSubscription()
        .expectComplete()
        .verify()
    }

    @Test
    fun givenErrorWhileRetrieving_shouldReturnErrorMono() {
      // Given
      every { partnerRepository.findById(any<String>()) } returns Mono.error(Exception("Boom!"))

      // When-Then
      StepVerifier
        .create(partnerService.getPartnerById(RANDOM.nextObject()))
        .expectSubscription()
        .expectErrorSatisfies {
          assertEquals(Exception::class, it::class)
          assertEquals("Boom!", it.message)
        }
        .verify()
    }
  }

  @Nested
  inner class CreatePartner {

    @Test
    fun givenExistingPartner_shouldReturnErrorMono() {}

    @Test
    fun givenNoExistingPartner_shouldReturnCreatedPartnerMono() {}

    @Test
    fun givenErrorWhileRetrieving_shouldReturnErrorMono() {}

    @Test
    fun givenErrorWhileSaving_shouldReturnErrorMono() {}
  }

  @Nested
  inner class GetAllPartnerAccounts {

    @Test
    fun givenPartnerWithAccounts_shouldReturnNonEmptyFlux() {}

    @Test
    fun givenPartnerWithoutAccount_shouldReturnEmptyFlux() {}

    @Test
    fun givenNoPartners_shouldReturnErrorFlux() {}

    @Test
    fun givenErrorWhileRetrieving_shouldReturnErrorFlux() {}
  }

  @Nested
  inner class UpdatePartner {

    @Nested
    inner class Email {

      @Test
      fun givenExistingPartner_andExistingEmail_shouldReturnOriginalPartnerMono() {}

      @Test
      fun givenExistingPartner_andNonExistingEmail_shouldReturnUpdatedPartnerMono() {}

      @Test
      fun givenNonExistingPartner_shouldReturnErrorMono() {}

      @Test
      fun givenErrorWhileRetrieving_shouldReturnErrorMono() {}

      @Test
      fun givenErrorWhileSaving_shouldReturnErrorMono() {}
    }

    @Nested
    inner class Accounts {

      @Test
      fun givenExistingPartner_andExistingAccount_shouldReturnOriginalPartnerMono() {}

      @Test
      fun givenExistingPartner_andNonExistingAccount_shouldReturnUpdatedPartnerMono() {}

      @Test
      fun givenNonExistingPartner_shouldReturnErrorMono() {}

      @Test
      fun givenErrorWhileRetrieving_shouldReturnErrorMono() {}

      @Test
      fun givenErrorWhileSaving_shouldReturnErrorMono() {}
    }
  }
}
