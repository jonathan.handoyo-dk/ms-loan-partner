package com.jago.loan.partner.services

import com.jago.loan.partner.controllers.dtos.PatchPartnerAccountRequest
import com.jago.loan.partner.controllers.dtos.PatchPartnerEmailRequest
import com.jago.loan.partner.domains.Partner
import com.jago.loan.partner.domains.PartnerRepository
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import reactor.kotlin.core.publisher.toMono

@Service
class PartnerService(
  private val partnerRepository: PartnerRepository,
) {

  fun getAllPartners(): Flux<Partner> {
    return partnerRepository.findAll()
  }

  fun getPartnerById(id: String): Mono<Partner> {
    return partnerRepository.findById(id)
  }

  fun createPartner(partner: Partner): Mono<Partner> {
    return partnerRepository.existsById(partner.id)
      .handle { found, sink ->
        when (found) {
          true -> sink.error(PartnerAlreadyExistsException(partner))
          else -> sink.next(partner)
        }
      }
      .flatMap { partnerRepository.save(it) }
  }

  fun updatePartner(id: String, request: PatchPartnerEmailRequest): Mono<Partner> {
    return partnerRepository.findById(id)
      .switchIfEmpty { PartnerNotExistsException(id).toMono() }
      .map { it.copy(email = it.email + request.email) }
      .flatMap { partnerRepository.save(it) }
  }

  fun updatePartner(id: String, request: PatchPartnerAccountRequest): Mono<Partner> {
    return partnerRepository.findById(id)
      .switchIfEmpty { PartnerNotExistsException(id).toMono() }
      .map { it.copy(accounts = (it.accounts ?: emptyList()) + request.account) }
      .flatMap { partnerRepository.save(it) }
  }
}

sealed class PartnerException(message: String) : Exception(message)
class PartnerAlreadyExistsException(partner: Partner) : PartnerException("Partner [id=${partner.id}] already exists")
class PartnerNotExistsException(id: String) : PartnerException("Partner [id=$id] does not exist")
class PartnerAccountNotExistsException(partner: Partner) : PartnerException("Partner [id=${partner.id}] has no accounts")
