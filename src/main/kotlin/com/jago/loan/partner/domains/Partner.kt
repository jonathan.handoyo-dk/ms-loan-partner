package com.jago.loan.partner.domains

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import java.math.BigDecimal
import java.time.LocalDate

@Document
data class Partner(
  @Id
  val id: String,
  val name: String,
  val code: String,
  val email: List<String> = emptyList(),
  val channel: Channel,
  val partnerSequenceNumber: String?,

  val maximumOutstandingAmount: BigDecimal? = null,
  val maximumDisbursementAmountPerBorrower: BigDecimal? = null,
  val minimumRepaymentAccountBalance: BigDecimal? = null,

  val isAllowOverbooking: Boolean = false,
  val effectiveDate: LocalDate? = null,
  val makeChangeRequestId: String? = null,

  val accounts: List<Account>? = null,
  val productGroupOutstandingLimits: List<Any>? = null,
  val productSchemas: List<Any>? = null,
  val policyCheckItems: List<Any>? = null,
  val loanApplication: Any? = null,
  val webhook: Any? = null,
  val branch: Any? = null,

  val createdBy: String? = null,
  val updatedBy: String? = null,
) {

  enum class Channel {
    LFS,
    JFS,
    LP,
  }

}

@Repository
interface PartnerRepository: ReactiveMongoRepository<Partner, String>
