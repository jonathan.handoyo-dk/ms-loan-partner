package com.jago.loan.partner.domains

data class Account(
    val type: Type,
    val number: String,
    val name: String,
) {

    enum class Type {
        DISBURSEMENT_ACCOUNT,
        REPAYMENT_ACCOUNT,
        TAKEOVER_ACCOUNT,
    }
}