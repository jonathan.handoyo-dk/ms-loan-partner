package com.jago.loan.partner.controllers.dtos

data class PatchPartnerEmailRequest(
  val email: String,
)
