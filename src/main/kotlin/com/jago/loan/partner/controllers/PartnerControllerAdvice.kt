package com.jago.loan.partner.controllers

import com.jago.loan.partner.services.PartnerAccountNotExistsException
import com.jago.loan.partner.services.PartnerAlreadyExistsException
import com.jago.loan.partner.services.PartnerNotExistsException
import org.springframework.core.env.Environment
import org.springframework.core.env.Profiles
import org.springframework.http.HttpStatus
import org.springframework.http.ProblemDetail
import org.springframework.http.server.reactive.ServerHttpRequest
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.reactive.result.method.annotation.ResponseEntityExceptionHandler
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toMono
import java.net.URI

@RestControllerAdvice
class PartnerControllerAdvice(private val environment: Environment) : ResponseEntityExceptionHandler() {

  private fun ProblemDetail.apply(exception: Exception, request: ServerHttpRequest): ProblemDetail {
    instance = request.uri
    type = URI.create(request.path.toString())
    detail = exception.message
    title = "${exception.javaClass.simpleName} - ${exception.message}"

    when (environment.acceptsProfiles(Profiles.of("prod"))) {
      true -> setProperty("exception", title)
      else -> setProperty("exception", exception)
    }

    return this
  }

  @ExceptionHandler(PartnerAlreadyExistsException::class)
  fun handleConflict(exception: Exception, request: ServerHttpRequest): Mono<ProblemDetail> {
    return ProblemDetail.forStatus(HttpStatus.CONFLICT).apply(exception, request).toMono()
  }

  @ExceptionHandler(
    PartnerNotExistsException::class,
    PartnerAccountNotExistsException::class
  )
  fun handleNotFound(exception: Exception, request: ServerHttpRequest): Mono<ProblemDetail> {
    return ProblemDetail.forStatus(HttpStatus.NOT_FOUND).apply(exception, request).toMono()
  }
}