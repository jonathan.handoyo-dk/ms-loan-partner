package com.jago.loan.partner.controllers

import com.jago.loan.partner.controllers.dtos.*
import com.jago.loan.partner.domains.Account
import com.jago.loan.partner.services.PartnerAccountNotExistsException
import com.jago.loan.partner.services.PartnerNotExistsException
import com.jago.loan.partner.services.PartnerService
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.switchIfEmpty
import reactor.kotlin.core.publisher.switchIfEmptyDeferred
import reactor.kotlin.core.publisher.toFlux
import reactor.kotlin.core.publisher.toMono

@RestController
@RequestMapping("/partners")
class PartnerController(
  private val partnerService: PartnerService,
) {

  @GetMapping
  fun getAllPartners(): Flux<GetPartnerResponse> {
    return partnerService.getAllPartners()
      .map { GetPartnerResponse(it) }
      .switchIfEmptyDeferred { PartnerNotExistsException("").toFlux<GetPartnerResponse>() }
  }

  @GetMapping("/{id}")
  fun getPartnerById(@PathVariable id: String): Mono<GetPartnerResponse> {
    return partnerService.getPartnerById(id)
      .map { GetPartnerResponse(it) }
      .switchIfEmpty { PartnerNotExistsException(id).toMono() }
  }

  @PostMapping
  fun postPartner(@RequestBody body: PostPartnerRequest): Mono<PostPartnerResponse> {
    return partnerService.createPartner(body.toPartner()).map { PostPartnerResponse(it) }
  }

  @PatchMapping("/{id}/email")
  fun patchPartnerEmail(@PathVariable id: String, @RequestBody body: PatchPartnerEmailRequest): Mono<PatchPartnerResponse> {
    return partnerService.updatePartner(id, body).map { PatchPartnerResponse(it) }
  }

  @GetMapping("/{id}/accounts")
  fun getAllPartnerAccounts(@PathVariable id: String): Flux<Account> {
    return partnerService.getPartnerById(id)
      .switchIfEmpty { PartnerNotExistsException(id).toMono() }
      .filter { !it.accounts.isNullOrEmpty() }
      .handle { partner, sink ->
        when (partner.accounts.isNullOrEmpty()) {
          true -> sink.error(PartnerAccountNotExistsException(partner))
          else -> sink.next(partner.accounts)
        }
      }
      .flatMapMany { it.toFlux() }
  }

  @PatchMapping("/{id}/accounts")
  fun patchPartnerAccount(@PathVariable id: String, @RequestBody body: PatchPartnerAccountRequest): Mono<PatchPartnerResponse> {
    return partnerService.updatePartner(id, body).map { PatchPartnerResponse(it) }
  }
}

