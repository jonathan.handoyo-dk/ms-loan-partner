package com.jago.loan.partner.controllers.dtos

import com.jago.loan.partner.domains.Partner

data class PatchPartnerResponse(
  val id: String,
  val name: String,
  val code: String,
  val email: List<String>,
  val channel: Partner.Channel,
  val partnerSequenceNumber: String?,
) {
  constructor(partner: Partner): this(
    id = partner.id,
    name = partner.name,
    code = partner.code,
    email = partner.email,
    channel = partner.channel,
    partnerSequenceNumber = partner.partnerSequenceNumber,
  )
}
