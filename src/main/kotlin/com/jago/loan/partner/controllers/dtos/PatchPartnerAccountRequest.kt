package com.jago.loan.partner.controllers.dtos

import com.jago.loan.partner.domains.Account

data class PatchPartnerAccountRequest(
  val account: Account,
)
