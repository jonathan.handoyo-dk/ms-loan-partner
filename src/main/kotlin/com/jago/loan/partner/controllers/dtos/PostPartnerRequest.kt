package com.jago.loan.partner.controllers.dtos

import com.jago.loan.partner.domains.Partner

data class PostPartnerRequest(
  val id: String,
  val name: String,
  val code: String,
  val email: List<String>,
  val channel: Partner.Channel,
  val partnerSequenceNumber: String?,
) {

  fun toPartner(): Partner = Partner(
    id = this.id,
    name = this.name,
    code = this.code,
    email = this.email,
    channel = this.channel,
    partnerSequenceNumber = this.partnerSequenceNumber,
  )
}