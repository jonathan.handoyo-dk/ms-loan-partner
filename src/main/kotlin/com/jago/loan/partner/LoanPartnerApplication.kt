package com.jago.loan.partner

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.runApplication

@SpringBootApplication
@ConfigurationPropertiesScan
class LoanPartnerApplication

fun main(args: Array<String>) {
  runApplication<LoanPartnerApplication>(*args)
}
