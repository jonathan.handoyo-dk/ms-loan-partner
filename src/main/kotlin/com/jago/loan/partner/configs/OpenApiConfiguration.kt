package com.jago.loan.partner.configs

import io.swagger.v3.oas.models.Components
import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.security.SecurityScheme
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment

@ConfigurationProperties("app.open-api")
data class OpenApiProperties(
  val title: String,
  val version: String,
  val description: String
)

@Configuration
class OpenApiConfiguration {

  @Bean
  fun openApi(properties: OpenApiProperties, environment: Environment): OpenAPI {
    return OpenAPI()
      .apply {
        info = Info()
          .title(properties.title)
          .version(properties.version)
          .description(properties.description)
        components = Components()
          .addSecuritySchemes(
            "apiKey",
            SecurityScheme()
              .type(SecurityScheme.Type.HTTP)
              .scheme("bearer")
              .bearerFormat("JWT")
          )
        /*if (!environment.acceptsProfiles(Profiles.of("local"))) {
          servers = listOf(
            Server()
              .url("/app-loan")
              .description("This is for ingress behind HTTPS only")
          )
        }*/
      }
  }
}
