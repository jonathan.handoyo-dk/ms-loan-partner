package com.jago.loan.partner.configs

import io.micrometer.observation.ObservationRegistry
import io.micrometer.observation.aop.ObservedAspect
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ObservabilityConfiguration {

  @Bean
  fun observedAspect(observationRegistry: ObservationRegistry): ObservedAspect {
    return ObservedAspect(observationRegistry)
  }
}
